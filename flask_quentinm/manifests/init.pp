class flask_quentinm {
  
  package { "apache2":
  ensure => installed,
 }

  package { "python":
    ensure => installed,
  }

  package { "pip":
    ensure => installed,

  }

  package {"flask":
    ensure => installed,
    require => package[python],
    }

  package {"mysql":
    ensure => installed,
    require => package[python],
}

exec { 'Start_flask':
    path => "/usr/bin:/usr/sbin:/bin",  
    command => "pip install flask",
    require => Package[python-pip],
  }

exec { 'Start_App':
    command => "python my_app.py",
    require => Package[flask],
 }


}
